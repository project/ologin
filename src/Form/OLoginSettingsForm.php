<?php

namespace Drupal\ologin\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures module settings.
 */
class OLoginSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ologin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'ologin.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config = $this->config('ologin.settings');

    // Weixin QR Connect.
    $form['ologin_weixin'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Wechat login'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    ];

    $form['ologin_weixin']['login_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show in user login form'),
      '#default_value' => $config->get('weixin.login_form'),
      '#description' => $this->t('Show a Wechat link in the user login form, or you can build your own link to "/ologin/weixin" for users to login with Wechat.'),
    ];

    $defaultCallbackUrl = Url::fromRoute('ologin.weixin_callback', [], ['absolute' => TRUE])->toString();
    $form['ologin_weixin']['callback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wechat Callback URL'),
      '#description' => $this->t('Default: :url', [
        ':url' => $defaultCallbackUrl,
      ]),
      '#default_value' => !empty($config->get('weixin.callback')) ? $config->get('weixin.callback') : $defaultCallbackUrl,
      '#required' => TRUE,
    ];

    $form['ologin_weixin']['appkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AppKey'),
      '#default_value' => $config->get('weixin.appkey'),
      '#description' => $this->t('Get your AppKey and AppSecret from :url', [
        ':url' => 'https://open.weixin.qq.com/',
      ]),
      '#required' => TRUE,
    ];

    $form['ologin_weixin']['appsecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AppSecret'),
      '#default_value' => $config->get('weixin.appsecret'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('ologin.settings')
      ->set('weixin.login_form', $values['ologin_weixin']['login_form'])
      ->set('weixin.callback', $values['ologin_weixin']['callback'])
      ->set('weixin.appkey', $values['ologin_weixin']['appkey'])
      ->set('weixin.appsecret', $values['ologin_weixin']['appsecret'])
      ->save();
    // Invalidate the cache of user login form to show/hide ologin link(s).
    $tags = ['rendered'];
    Cache::invalidateTags($tags);
  }

}
