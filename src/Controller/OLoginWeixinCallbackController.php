<?php

namespace Drupal\oLogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * OLoginWeixinCallbackController.
 */
class OLoginWeixinCallbackController extends ControllerBase {

  /**
   * Main function.
   */
  public function main() {
    // Disable cache to allow dynamic redirect.
    \Drupal::service('page_cache_kill_switch')->trigger();

    $output = '';

    if (!\Drupal::request()->get('code')) {
      $message = $this->t('Error occurred, please retry later.');
      $this->messenger()->addError($message);
      \Drupal::logger('ologin')->error($this->t("Missing GET argument: code"));
      return ['#markup' => $message];
    }

    $code = \Drupal::request()->get('code');

    $url = $this->getAccessTokenUrl($code);

    // Get Access Token.
    $client = \Drupal::httpClient();
    $request = $client->request('POST', $url);
    $response = $request->getBody()->getContents();
    $data = Json::decode($response);

    // Error handle.
    if (isset($data['errcode'])) {
      $message = $this->t('Error code: @errcode, Error: @errmsg', [
        '@errcode' => $data['errcode'],
        '@errmsg' => $data['errmsg'],
      ]);
      $this->messenger()->addError($message);
      \Drupal::logger('ologin')->error($message);
      return ['#markup' => $message];
    }

    // Login.
    if (isset($data['unionid']) || isset($data['openid'])) {
      $ouid = isset($data['unionid']) ? $data['unionid'] : $data['openid'];
      return ologin_login($ouid, 'weixin', $data);
    }

    return ['#markup' => $output];
  }

  /**
   * Get access token.
   */
  private function getAccessTokenUrl($code) {
    $uri = 'https://api.weixin.qq.com/sns/oauth2/access_token';

    $appid = \Drupal::config('ologin.settings')->get('weixin.appkey');
    $secret = \Drupal::config('ologin.settings')->get('weixin.appsecret');

    $options = [
      'query' => [
        'appid'         => $appid,
        'secret'        => $secret,
        'grant_type'    => 'authorization_code',
        'code'          => $code,
      ],
    ];
    $url = Url::fromUri($uri, $options)->toString();
    return $url;
  }

}
