<?php

namespace Drupal\ologin\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * OLogin EventSubscriber.
 */
class OLoginSubscriber implements EventSubscriberInterface {

  /**
   * Redirect users to /user/{user}/edit page in conditions.
   */
  public function checkUserEmail(RequestEvent $event) {

    $disallowedRouteNames = [
      'entity.node.canonical',
    ];

    // Redirect in following condition:
    // - User is logged in
    // - Account has no mail
    // - In disallowed pages.
    if (\Drupal::currentUser()->isAuthenticated()
      && empty(\Drupal::currentUser()->getEmail())
      && in_array(\Drupal::routeMatch()->getRouteName(), $disallowedRouteNames)
    ) {
      // Build target url.
      $url = Url::fromRoute('entity.user.edit_form', [
        'user' => \Drupal::currentUser()->id(),
      ]);
      $url->setOption('query', [
        'destination' => \Drupal::request()->getRequestUri(),
      ]);
      // Redirect.
      $event->setResponse(new RedirectResponse($url->toString()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkUserEmail'];
    return $events;
  }

}
