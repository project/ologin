<?php

/**
 * @file
 * Main file for oLogin module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ologin_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $account = $form_state->getFormObject()->getEntity();
  // For account without mail, disable current_pass field and make password
  // fields required.
  if (empty($account->mail->value)) {
    \Drupal::messenger()->addWarning(t('Please set email address and password for your account.'));
    $form_state->set('user_pass_reset', TRUE);
    $form['account']['current_pass']['#access'] = FALSE;
    $form['account']['pass']['#required'] = TRUE;
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function ologin_user_delete($account) {
  \Drupal::database()->delete('ologin')
    ->condition('uid', $account->get('uid')->value)
    ->execute();
}

/**
 * User login function of oLogin.
 */
function ologin_login($ouid, $type, $data = []) {
  // Check bind.
  $result = \Drupal::database()->select('ologin', 'o')
    ->fields('o', ['uid'])
    ->condition('type', $type)
    ->condition('ouid', $ouid)
    ->execute();
  $uid = $result->fetchField();

  // Load binded account.
  if ($uid) {
    $account = \Drupal::service('entity_type.manager')->getStorage('user')->load($uid);
  }
  // Login a new register drupal user.
  else {
    // Temporary username.
    $_name = $ouid . time();

    // Create a new user.
    $account = User::create();
    $account->setUsername($_name);
    $password = \Drupal::service('password_generator')->generate();
    $account->setPassword($password);
    $account->enforceIsNew();
    $account->activate();
    $account->set('init', $type . '_' . $_name);
    $result = $account->save();

    // Terminate if an error occurred during user_save().
    if (!$result) {
      \Drupal::messenger()->addError(t("Error saving user account."));
      return;
    }

    // Get uid and set drupal username as u[uid].
    $uid = $account->get('uid')->value;
    $name = ologin_get_unique_username('u' . $uid);

    $account->setUsername($name);
    $account->save();

    // Add ologin record.
    ologin_set_record($uid, $type, $ouid);
  }

  $is_blocked = !$account->get('status')->value;

  if ($is_blocked) {
    \Drupal::messenger()->addError('This account is not activated or has been blocked');
  }
  else {
    // Load user object and process login action.
    // @see user_external_login_register
    // @see user_login_submit
    $user = \Drupal::service('entity_type.manager')->getStorage('user')->load($uid);
    user_login_finalize($user);
    $user_destination = Url::fromRoute('user.page')->toString();
    $response = new RedirectResponse($user_destination);
    return $response;
  }
}

/**
 * Generate unique username.
 */
function ologin_get_unique_username($seed_name, $name = NULL, $i = 1) {

  // Set $seed_name as default name.
  if (empty($name)) {
    $name = $seed_name;
  }

  // Check username.
  if (ologin_username_exist($name)) {
    // Add suffix if username exists.
    $name = $seed_name . '_' . $i;
    $i++;
    // Continue check if new name exists.
    $name = ologin_get_unique_username($seed_name, $name, $i);
  }

  // Return non-exists username.
  return $name;
}

/**
 * Check if a username exists.
 */
function ologin_username_exist($name) {
  $count = \Drupal::database()->select('users_field_data', 'u')
    ->condition('name', $name)
    ->countQuery()
    ->execute()
    ->fetchField();
  return $count;
}

/**
 * Add new oLogin record.
 */
function ologin_set_record($uid, $type, $name, $data = NULL) {
  \Drupal::database()->insert('ologin')
    ->fields([
      'uid' => $uid,
      'type' => $type,
      'ouid' => $name,
    ])
    ->execute();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ologin_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  $config = \Drupal::config('ologin.settings');
  if ($config->get('weixin.login_form')) {
    $form['ologin'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="ologin-wrapper"><a href="{{ url }}" class="ologin-weixin" title="{% trans %}Login with Wechat"{% endtrans %}><img src="https://open.weixin.qq.com/zh_CN/htmledition/res/assets/res-design-download/icon24_wx_button.png" alt="" data-width="117" data-ratio="0.20512820512820512"></a></div>',
      '#context' => [
        'url' => \Drupal::urlGenerator()->generateFromRoute('ologin.weixin'),
      ],
    ];
  }
}
