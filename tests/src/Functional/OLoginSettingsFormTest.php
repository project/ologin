<?php

namespace Drupal\Tests\mix\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests oLogin module.
 *
 * @group ologin
 */
class OLoginSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Authenticated user account.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $authUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ologin'];

  /**
   * Test settings form.
   *
   * @covers SettingsForm::buildForm
   */
  public function testSettingsForm() {

    // Test account with config admin permission.
    $configAdminUser = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($configAdminUser);
    $this->drupalGet('/admin/config/system/ologin');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('Show in user login form');
    $this->assertSession()->fieldExists('Wechat Callback URL');
    $this->assertSession()->fieldExists('AppKey');
    $this->assertSession()->fieldExists('AppSecret');

    // Assert default form values.
    $this->assertSession()->fieldValueEquals('Show in user login form', FALSE);
    $this->assertSession()->fieldValueEquals('Wechat Callback URL', Url::fromRoute('ologin.weixin_callback', [], ['absolute' => TRUE])->toString());
    $this->assertSession()->fieldValueEquals('AppKey', '');
    $this->assertSession()->fieldValueEquals('AppSecret', '');

    // Submit form.
    $edit = [];

    $edit['ologin_weixin[login_form]'] = TRUE;
    $edit['ologin_weixin[appkey]']     = '1111111111';
    $edit['ologin_weixin[appsecret]']  = 'xxxxxxxxxx';

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    // Assert new values.
    $this->assertSession()->fieldValueEquals('Show in user login form', TRUE);
    $this->assertSession()->fieldValueEquals('Wechat Callback URL', Url::fromRoute('ologin.weixin_callback', [], ['absolute' => TRUE])->toString());
    $this->assertSession()->fieldValueEquals('AppKey', '1111111111');
    $this->assertSession()->fieldValueEquals('AppSecret', 'xxxxxxxxxx');

    // Assert user login form.
    $this->drupalLogout();
    $this->drupalGet('/user/login');
    $this->assertSession()->responseContains('class="ologin-wrapper"');
    $this->assertSession()->responseNotContains('http://example.com');

    // Hide ologin links and assert user login form again.
    $this->config('ologin.settings')
      ->set('weixin.callback', 'http://example.com')
      ->save();
    Cache::invalidateTags(['rendered']);
    $this->drupalGet('/user/login');
    $this->assertSession()->responseContains('http://example.com');

    $this->config('ologin.settings')
      ->set('weixin.login_form', FALSE)
      ->save();
    Cache::invalidateTags(['rendered']);

    $this->drupalGet('/user/login');
    $this->assertSession()->responseNotContains('class="ologin-wrapper"');
  }

}
